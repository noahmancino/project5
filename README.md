# Brevet time calculator with Flask and Ajax

A webapp in flask and ajax implimenting the algorithm for brevet times described 
[here](https://rusa.org/pages/acp-brevet-control-times-calculator). Simply
open up the webpage and input the date, total brevet length, and your control's
distance (in kms or miles) from the starting line. The app will then display 
the appropriate opening and closing times for your control. If, for some
reason, you want to see the result displayed on another page there's a
button to submit your times and another button to view the page.
I don't know why you would want to do that though.

## How times are calculated.

### "Regular" times 
Times for controls before the end of the brevet and after the starting point 
are calculated by dividing the race into specifed portions, dividing the 
portions by the maximum (for open time) and minimum (for close time) allowed 
speeds. Input kilometers and minutes are rounded to the nearest natural number.  


| Portion: km       | Minimum speed: km/h    | Maximum speed: km/h    |
|-------------------|------------------------|------------------------|
| 0-200		    | 15		     | 34                     |
| 201-400           | 15                     | 32                     |
| 401-600           | 15                     | 30                     |
| 601-1000          | 11.428                 | 28                     |

example:  
date: 00/00/00T00:00
control distance: 550  
total brevet: 600  

portion 1: 0-200  
portion 2: 201-400  
portion 3: 401-550  

Open time: 00/00/00T17:08  
Close time: 00/00/01T12:48

### Special cases

If a control is more than 20 percent greater in distance than the nominal brevet
, no output will be generated. If a control is on a brevet, at 0 kms, or after a 
brevet within the acceptable range, refer to the nearest entry in the provided 
table.  


| Brevet  | Open time: HH:MM | Close time: HH:MM |
|---------|------------------|-------------------|
| 0	  | 00:00	     | 01:00             |
| 200 km  | 05:53            | 13:30             |
| 400 km  | 12:08            | 27:00             |
| 600 km  | 18:48            | 40:00             |
| 1000 km | 33:45            | 75:00             |